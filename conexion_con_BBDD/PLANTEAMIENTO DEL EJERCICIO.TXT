WEB API EMPLEADOS CORE 
 
Vamos a crear un Web Api de acceso a datos utilizando Net Core. 
Utilizar MVC tradicional o Core no tiene casi ninguna diferencia, pero así aprenderemos a configurar nuestro Api. 
Utilizaremos la base de datos de Hospital de Azure y los empleados. 
 
 
 
Comenzamos creando un proyecto nuevo MVC Core llamado ApiEmpleadosCore con la plantilla Empty. 
 
 
 
Sobre el proyecto, agregamos NuGet de Entity Framework 
 
 
 
Debemos crear las carpetas Models y Controllers. 
 
Sobre la carpeta Models, creamos una nueva clase llamada Empleado 
 
EMPLEADO 
 
using System; 
using System.Collections.Generic; 
using System.ComponentModel.DataAnnotations; 
using System.ComponentModel.DataAnnotations.Schema; 
using System.Linq; 
using System.Threading.Tasks; 
 
namespace ApiEmpleadosCore.Models 
{ 
    [Table("EMP")] 
    public class Empleado 
    { 
        [Key] 
        [Column("EMP_NO")] 
        public int IdEmpleado { get; set; } 
        [Column("APELLIDO")] 
        public String Apellido { get; set; } 
        [Column("OFICIO")] 
        public String Oficio { get; set; } 
        [Column("SALARIO")] 
        public int Salario { get; set; } 
        [Column("DEPT_NO")] 
        public int Departamento { get; set; } 
    } 
} 
 
Creamos una nueva carpeta llamada Data y agregamos una clase llamada EmpleadosContext. 
 
EMPLEADOSCONTEXT 
 
using ApiEmpleadosCore.Models; 
using Microsoft.EntityFrameworkCore; 
using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Threading.Tasks; 
 
namespace ApiEmpleadosCore.Data 
{ 
    public class EmpleadosContext: DbContext 
    { 
        public EmpleadosContext(DbContextOptions options): base(options) 
        { } 
 
        public DbSet<Empleado> Empleados { get; set; } 
    } 
} 
 
Agregamos una nueva carpeta llamada Repositories y una clase llamada RepositoryEmpleados. 
 
REPOSITORYEMPLEADOS 
 
using ApiEmpleadosCore.Data; 
using ApiEmpleadosCore.Models; 
using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Threading.Tasks; 
 
namespace ApiEmpleadosCore.Repositories 
{ 
    public class RepositoryEmpleados 
    { 
        EmpleadosContext context; 
 
        public RepositoryEmpleados(EmpleadosContext context) 
        { 
            this.context = context; 
        } 
 
        public List<Empleado> GetEmpleados() 
        { 
            return this.context.Empleados.ToList(); 
        } 
 
        public Empleado BuscarEmpleado(int idempleado) 
        { 
            return this.context.Empleados.SingleOrDefault(z => z.IdEmpleado == idempleado); 
        } 
    } 
} 
 
Sobre la carpeta Controllers, agregamos un nuevo controlador de tipo Web Api Empty llamado EmpleadosController. 
 
 
 
EMPLEADOSCONTROLLER 
 
using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Threading.Tasks; 
using ApiEmpleadosCore.Models; 
using ApiEmpleadosCore.Repositories; 
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc; 
 
namespace ApiEmpleadosCore.Controllers 
{ 
    [Route("api/[controller]")] 
    [ApiController] 
    public class EmpleadosController : ControllerBase 
    { 
        RepositoryEmpleados repo; 
 
        public EmpleadosController(RepositoryEmpleados repo) 
        { 
            this.repo = repo; 
        } 
 
        [HttpGet] 
        public ActionResult<List<Empleado>> Get() 
        { 
            return this.repo.GetEmpleados(); 
        } 
 
        [HttpGet("{id}")] 
        public ActionResult<Empleado> Get(int id) 
        { 
            return this.repo.BuscarEmpleado(id); 
        } 
    } 
} 
 
Sobre el proyecto, añadimos un nuevo archivo de settings e incluimos la cadena de conexión a Azure. 
 
 
 
APPSETTINGS.JSON 
 
{ 
  "ConnectionStrings": { 
    "cadenahospitalazure": "Data Source=sqlxamarin.database.windows.net;Initial Catalog=HOSPITALXAMARIN;Persist Security Info=True;User ID=adminsql; password=Admin123" 
  } 
} 
 
Ahora abrimos el archivo Startup.cs y resolvemos las dependencias en el método ConfigureServices(). 
 
STARTUP.CS 
 
using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Threading.Tasks; 
using ApiEmpleadosCore.Data; 
using ApiEmpleadosCore.Repositories; 
using Microsoft.AspNetCore.Builder; 
using Microsoft.AspNetCore.Hosting; 
using Microsoft.AspNetCore.Http; 
using Microsoft.EntityFrameworkCore; 
using Microsoft.Extensions.Configuration; 
using Microsoft.Extensions.DependencyInjection; 
 
namespace ApiEmpleadosCore 
{ 
    public class Startup 
    { 
 
        public IConfiguration Configuration { get; } 
 
        public Startup(IConfiguration configuration) 
        { 
            Configuration = configuration; 
        } 
 
        public void ConfigureServices(IServiceCollection services) 
        { 
            String cadenaconexion = 
   Configuration.GetConnectionString("cadenahospitalazure"); 
 
            services.AddTransient<RepositoryEmpleados>(); 
 
            services.AddDbContext<EmpleadosContext>(options => 
        options.UseSqlServer(cadenaconexion)); 
 
            services.AddMvc(); 
        } 
 
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) 
        { 
            if (env.IsDevelopment()) 
            { 
                app.UseDeveloperExceptionPage(); 
            } 
 
            app.UseMvc(); 
        } 
    } 
} 
 
A continuación, vamos a modificar la aplicación e incluir más métodos para mapear las peticiones GET. 
Incluimos en la clase Repository un par de métodos para mostrar los empleados por salario y buscar los empleados por varios parámetros, como por ejemplo, el oficio y el departamento. 
 
        public List<Empleado> GetEmpleadosSalario(int salario) 
        { 
            return this.context.Empleados.Where(x => x.Salario >= salario).ToList(); 
        }  
 
        public List<Empleado> GetEmpleados(String oficio, int departamento) 
        { 
            var consulta = from datos in context.Empleados 
                           where datos.Oficio == oficio 
                           && datos.Departamento == departamento 
                           select datos; 
            return consulta.ToList(); 
        } 
 
Abrimos EmpleadosController e incluimos los nuevos métodos para el Api. 
 
        [HttpGet("{oficio}/{departamento}")] 
        public ActionResult<List<Empleado>> GetEmpleados(String oficio, int departamento) 
        { 
            return this.repo.GetEmpleados(oficio, departamento); 
        } 
 
        [Route("[action]/{salario}")] 
        [HttpGet] 
        public ActionResult<List<Empleado>> GetEmpleadosSalario(int salario) 
        { 
            return this.repo.GetEmpleadosSalario(salario); 
        } 
 
Y podremos visualizar nuestra aplicación con las peticiones correctas. 
 
https://localhost:44307/api/Empleados/GetEmpleadosSalario/400000 
 
 
 
https://localhost:44307/api/Empleados/ANALISTA/20 
 
 
 
 
 
